# Homebridge-EcoFlow plugin

Uses MQTT informations to retrieve battery percentage as a lightbulb.
The battery percentage is used as the lightbulb brightness, so that it let perform automation.
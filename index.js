"use strict"

var Service, Characteristic, HomebridgeAPI
const mqtt = require('mqtt')
const https = require('https')

const ECOFLOW_API_LOGIN = 'https://api.ecoflow.com/auth/login'
const ECOFLOW_API_CERTIFICATION = 'https://api.ecoflow.com/iot-auth/app/certification'
const ECOFLOW_MQTT = 'mqtts://mqtt-e.ecoflow.com'
const ECOFLOW_MQTT_PORT = 8883
const ECOFLOW_MQTT_TOPIC = '/app/device/property/'
const minute = 60000

module.exports = function (homebridge) {
  Service = homebridge.hap.Service
  Characteristic = homebridge.hap.Characteristic
  HomebridgeAPI = homebridge
  homebridge.registerAccessory("homebridge-ecoflow", "EcoFlow", EcoFlowLite)
}

function EcoFlowLite(log, config) {
  //-- Base informations
  this.log = log
  this.name = config.name
  this.model = config.model
  this.serialNumber = config.serialNumber
  this.firmwareVersion = config.firmwareVersion
  this.computeWH = config.computeWHEQ || false

  //-- Properties
  this.restartMQTTCount = 0
  this.mqttClientId = null
  this.mainSoC = 100
  this.additionnalSoC = 100
  this.soc = 100 // state of charge (%)
  this.cow = 0 // current output power (W)
  this.tbu = 0 // total battery usage (Wh)
  this.tow = 0 // total output power (Wh)
  this.twg = 0 // total output power without grid (Wh)
  this.spp = 0 // solar power production (Wh)
  this.tsp = 0 // total solar power production (kWh)
  this.csp = 0 // current solar production (W)
  this.csp1 = 0 // current solar panel 1 production (W)
  this.csp2 = 0 // current solar panel 2 production (W)
  this.csp3 = 0 // current solar panel Powerstream production (W)
  this.bcc0 = 0 // main battery cycles count
  this.bcc1 = 0 // second battery cycles count
  this.mqttLastMessageDate = 0

  //-- Storage
  if (config.cache) {
    this.cacheDirectory = HomebridgeAPI.user.persistPath()
    this.storage = require('node-persist')
    this.storage.initSync({ dir: this.cacheDirectory, forgiveParseErrors: true })
    this.socStorageKey = this.name + "SoC"
    this.sppStorageKey = this.name + "SpP"
    this.tspStorageKey = this.name + "TsP"
    this.towStorageKey = this.name + "ToW"
    this.tbuStorageKey = this.name + "TbU"
    this.twgStorageKey = this.name + "TwG"

    this.soc = this.storage.getItemSync(this.socStorageKey) || 100
    this.soc = +this.soc
    this.spp = this.storage.getItemSync(this.sppStorageKey) || 0
    this.spp = +this.spp
    this.tsp = this.storage.getItemSync(this.tspStorageKey) || 0
    this.tsp = +this.tsp
    this.tow = this.storage.getItemSync(this.towStorageKey) || 0
    this.tow = +this.tow
    this.tbu = this.storage.getItemSync(this.tbuStorageKey) || 0
    this.tbu = +this.tbu
    this.twg = this.storage.getItemSync(this.twgStorageKey) || 0
    this.twg = +this.twg
  }

  //-- Homekit accessory
  //// Information
  this.informationService = new Service.AccessoryInformation()
  this.informationService
    .setCharacteristic(Characteristic.Manufacturer, 'EcoFlow')
    .setCharacteristic(Characteristic.Model, this.model)
    .setCharacteristic(Characteristic.Name, this.name)
    .setCharacteristic(Characteristic.SerialNumber, this.serialNumber)
    .setCharacteristic(Characteristic.FirmwareRevision, this.firmwareVersion)

  //// Services
  // Battery level: simulate a diming lightbulb. The percentage of the lightbulb is the power level of the battery
  this.batteryLevelService = new Service.Lightbulb(this.name)
  this.batteryLevelService.getCharacteristic(Characteristic.On)
    .on('get', this._getChargingState.bind(this))
  this.batteryLevelService.getCharacteristic(Characteristic.Brightness)
    .on('get', this._getPower.bind(this))
  this.batteryLevelService.setCharacteristic(Characteristic.On, true)

  // Battery level automation: simulate a light sensor. The lux value measured by the light sensor is the current soc. This let perform automation on battery level.
  this.batteryLevelAutomationService = new Service.LightSensor('Niveau de batterie', 'soc')
  this.batteryLevelAutomationService.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
    .on('get', this._getPower.bind(this))

  // Solar power current production: simulate a light sensor. The lux value measured by the light sensor is the current total W input
  this.solarPowerCurrentProduction = new Service.LightSensor('Production solaire instantanée', 'csp')
  this.solarPowerCurrentProduction.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
    .on('get', this._getSolarPowerCurrentProduction.bind(this))

  // Solar power current production on PV1: simulate a light sensor. The lux value measured by the light sensor is the current W input on PV1
  this.pv1CurrentProduction = new Service.LightSensor('Entrée PV1', 'csp1')
  this.pv1CurrentProduction.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
    .on('get', this._getPV1CurrentProduction.bind(this))

  // Solar power current production on PV2: simulate a light sensor. The lux value measured by the light sensor is the current W input on PV2
  this.pv2CurrentProduction = new Service.LightSensor('Entrée PV2', 'csp2')
  this.pv2CurrentProduction.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
    .on('get', this._getPV2CurrentProduction.bind(this))

  // Solar power current production from Powerstream and used by battery: simulate a light sensor. The lux value measured by the light sensor is the current W input from Powerstream
  this.pv3CurrentProduction = new Service.LightSensor('Entrée PV Powerstream', 'csp3')
  this.pv3CurrentProduction.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
    .on('get', this._getPV3CurrentProduction.bind(this))

  // Outpout power consumption: simulate a light sensor. The lux value measured by the light sensor is the current W output
  this.outputPowerConsumption = new Service.LightSensor('Sortie', 'cow')
  this.outputPowerConsumption.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
    .on('get', this._getOutputPower.bind(this))

  // Battery cycle count: simulate a light sensor. The lux value measured by the light sensor is the current cycle count
  this.cycleCountBattery0 = new Service.LightSensor('Cycles - batterie principale', 'bca')
  this.cycleCountBattery0.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
    .on('get', this._getCycleCountBattery0.bind(this))

  this.cycleCountBattery1 = new Service.LightSensor('Cycles - batterie additionnelle 1', 'bcb')
  this.cycleCountBattery1.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
    .on('get', this._getCycleCountBattery1.bind(this))

  if (this.computeWH) {
    // Total solar power production: simulate a light sensor. The lux value measured by the light sensor is the kWH of all time
    this.totalSolarPowerProduction = new Service.LightSensor('Production solaire totale', 'tsp')
    this.totalSolarPowerProduction.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
      .on('get', this._getTotalSolarPowerProduction.bind(this))

    // Solar power production: simulate a light sensor. The lux value measured by the light sensor is the wH of the current day
    this.solarPowerProduction = new Service.LightSensor('Production solaire', 'spp')
    this.solarPowerProduction.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
      .on('get', this._getSolarPowerProduction.bind(this))
    
    // Total outpout power consumption: simulate a light sensor. The lux value measured by the light sensor is the current Wh output
    this.totalOutputPowerConsumption = new Service.LightSensor('Consommation', 'tow')
    this.totalOutputPowerConsumption.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
      .on('get', this._getTotalOutputPower.bind(this))

    // Total battery usage: simulate a light sensor. The lux value measured by the light sensor is the Wh battery usage
    this.totalBatteryUsage = new Service.LightSensor('Consommation sur batterie', 'tbu')
    this.totalBatteryUsage.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
      .on('get', this._getTotalBatteryUsage.bind(this))

    // Total ouput without grid consumption: simulate a light sensor. The lux value measured by the light sensor is the Wh output
    this.totalWithoutGrid = new Service.LightSensor('Consommation sans Enedis', 'twg')
    this.totalWithoutGrid.getCharacteristic(Characteristic.CurrentAmbientLightLevel)
      .on('get', this._getTotalOutputPowerWithoutGrid.bind(this))
  }

  //-- MQTT
  setInterval(() => {
    const currentDate = new Date()
    // Notify every 10 minutes
    if (currentDate.getMinutes() % 10 === 0) this.notify()

    // Reset values on new day
    if (this.isNewDay(currentDate)) {
      this.log.info(`This is a new day, restart connection`)
      this.spp = 0
      this.tow = 0
      this.tbu = 0
      this.twg = 0
      this.restartMQTTCount = 0
      this.mqttClientId = null
    }

    // Save values in cache
    if (config.cache) {
      this.storage.setItemSync(this.socStorageKey, this.soc)
      this.storage.setItemSync(this.sppStorageKey, this.spp)
      this.storage.setItemSync(this.tspStorageKey, this.tsp)
      this.storage.setItemSync(this.towStorageKey, this.tow)
      this.storage.setItemSync(this.tbuStorageKey, this.tbu)
      this.storage.setItemSync(this.twgStorageKey, this.twg)
    }

    // Restart connection if last message is too old
    if (currentDate.getTime() - this.mqttLastMessageDate > 5*minute ) {
      this.log.info("Last important message is from 5 minutes ago or more, restart connection")
      this.mqttClientId = null
    }

    // Restart connection
    if (!this.mqttClientId) {
      this.mqtt.end(true)
      this.connectEcoFlowAPI()
    } 
  }, minute)

  this.offGridHours = () => {
    const currentDate = new Date()
    return currentDate.getHours() >= 6 && currentDate.getHours() < 22
  }

  this.isNewDay = (currentDate) => {
    return currentDate.getHours() === 0 && currentDate.getMinutes() === 0
  }

  this.notify = () => {
    this.batteryLevelService.setCharacteristic(Characteristic.Brightness, this.soc)
    this.batteryLevelAutomationService.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.soc || 0.0001)
    this.cycleCountBattery0.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.bcc0 || 0.0001)
    this.cycleCountBattery1.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.bcc1 || 0.0001)
    this.outputPowerConsumption.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.cow || 0.0001)
    this.solarPowerCurrentProduction.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.csp || 0.0001)
    this.pv1CurrentProduction.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.csp1 || 0.0001)
    this.pv2CurrentProduction.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.csp2 || 0.0001)
    this.pv3CurrentProduction.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.csp3 || 0.0001)

    if (this.computeWH) {
      this.totalSolarPowerProduction.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.tsp || 0.0001)
      this.solarPowerProduction.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.spp || 0.0001)
      this.totalOutputPowerConsumption.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.tow || 0.0001)
      this.totalBatteryUsage.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.tbu || 0.0001)
      this.totalWithoutGrid.setCharacteristic(Characteristic.CurrentAmbientLightLevel, this.twg || 0.0001)
    }
  }

  this.performAPIRequest = (url, method, data, token = null) => {
    return new Promise((resolve, reject) => {
      const options = {
        method,
        headers: { 
          'Accept': 'application/json',
          'Content-Type': 'application/json' 
        },
      }

      if (token) options.headers["Authorization"] = "Bearer "+token 
  
      const responseCallback = (response) => {
        let data = ''
        response.on('data', (dataChunk) => {
            data += dataChunk
        })
        response.on('end', async () => {
            let body
            try {
                body = JSON.parse(data)
            } catch (err) /* istanbul ignore next */ {
                this.log.error(`Could not parse response data - ${data.length ? data : 'no data'}`)
            }
  
            resolve(body)
        })
        response.on('error', async error => {
            reject(error)
        })
    }
  
      const req = https.request(url, options, responseCallback)
      req.write(JSON.stringify(data))
      req.end()
    })
  }

  this.connectEcoFlowAPI = () => {
    const data = {
      "os": "linux",
      "scene": "IOT_APP",
      "appVersion": "1.0.0",
      "osVersion": "5.15.90.1-kali-fake",
      "password": Buffer.from(config.credentials.password).toString('base64'),
      "oauth": {
        "bundleId": "com.ef.EcoFlow"
      },
      "email": config.credentials.email,
      "userType": "ECOFLOW"
    }

    this.performAPIRequest(ECOFLOW_API_LOGIN, "POST", data)
    .then(loginResponse => {
      this.ecoflowUserId = loginResponse.data.user.userId
      return this.performAPIRequest(ECOFLOW_API_CERTIFICATION, "GET", null, loginResponse.data.token)
    })
    .then(certificationResponse => {
        this.connectEcoFlowMQTT(this.ecoflowUserId, certificationResponse.data.certificateAccount, certificationResponse.data.certificatePassword)
    })
    .catch(err => {
      this.log.error(err)
    })
  }

  this.connectEcoFlowMQTT = (userid, username, password) => {
    this.restartMQTTCount++
    this.log.info(`connectEcoFlowMQTT - ${this.restartMQTTCount} time(s) today`)
    const date = new Date()
    date.setHours(Math.floor(date.getHours()/6))
    date.setMinutes(0)
    date.setSeconds(0)
    date.setMilliseconds(0)
    this.mqttClientId = "ANDROID_" + (Math.floor(date.getTime() / 100000)) + "_" + userid
    this.mqtt = mqtt.connect(`${ECOFLOW_MQTT}`, {
      username,
      password,
      clientId: this.mqttClientId,

      keepalive: 120,
      protocolId: 'MQTT',
      protocolVersion: 4,
      clean: true,
      reconnectPeriod: 60000,
      connectTimeout: 60000,
    })
    this.mqtt.on('connect', () => {
      this.mqttLastMessageDate = (new Date()).getTime()
      this.mqtt.subscribe(`${ECOFLOW_MQTT_TOPIC}${config.serialNumber}`, (err) => {
        if (err) {
          this.log.warn('Could not subscribe to device topic')
          this.mqttClientId = null
        }
      })
    })
    this.mqtt.on('message', (topic, message) => {
      if (topic === `${ECOFLOW_MQTT_TOPIC}${config.serialNumber}`) {
        try {
          const jsonData = JSON.parse(message)
          if (jsonData && jsonData.params) {
            const currentTime = (new Date()).getTime()
            const payload = jsonData.params
            const propsReceived = Object.keys(payload)
            let notify = false

            //// Properties update
            // Cycles count
            if (propsReceived.includes("bms_bmsStatus.cycles")) {
              this.bcc0 = payload['bms_bmsStatus.cycles']
            }
            if (propsReceived.includes("bms_slave_bmsSlaveStatus_1.cycles")) {
              this.bcc1 = payload['bms_slave_bmsSlaveStatus_1.cycles']
            }
            // Current battery level
            if (propsReceived.includes("bms_bmsStatus.f32ShowSoc") || propsReceived.includes("bms_slave_bmsSlaveStatus_1.f32ShowSoc")) {
              this.mainSoC = payload["bms_bmsStatus.f32ShowSoc"] || this.mainSoC
              this.additionnalSoC = payload["bms_slave_bmsSlaveStatus_1.f32ShowSoc"] || this.additionnalSoC
              const soc = (this.mainSoC + this.additionnalSoC)/2
              notify = soc.toFixed(1) !== this.soc.toFixed(1)
              this.soc = soc
            }
            // Output power
            if (propsReceived.includes("pd.wattsOutSum")) {
              this.cow = payload["pd.wattsOutSum"]
              // additionnal components management: (-) -> additionnal gives to main / (+) -> main gives to additionnal
              if (propsReceived.includes('pd.XT150Watts1') && payload['pd.XT150Watts1'] > 0) {
                // Additionnal battery
                this.cow -= Math.abs(payload['pd.XT150Watts1'])
              }
            }
            // Solar power incomming
            if ((propsReceived.includes("mppt.inVol") && propsReceived.includes("mppt.inAmp"))
               || (propsReceived.includes("mppt.pv2InVol") && propsReceived.includes("mppt.pv2InAmp"))
               || propsReceived.includes('pd.XT150Watts2')) {

              // PV1
            if (propsReceived.includes("mppt.inVol") && propsReceived.includes("mppt.inAmp")) {
                this.csp1 = (payload['mppt.inVol'] || 0)/1000 * (payload['mppt.inAmp'] || 0)/1000
              }

              // PV2
              if (propsReceived.includes("mppt.pv2InVol") && propsReceived.includes("mppt.pv2InAmp")) {
                this.csp2 = (payload['mppt.pv2InVol'] || 0)/1000 * (payload['mppt.pv2InAmp'] || 0)/1000
              }

              // PowerStream
              if (propsReceived.includes('pd.XT150Watts2')) {
                if (payload['pd.XT150Watts2'] <= 0) {
                  this.csp3 = Math.abs(payload['pd.XT150Watts2'])
                  if (config.powerStreamHomeInjection && this.csp3 >= 1) {
                    this.csp3 += config.powerStreamHomeInjection
                  }
                } else {
                  this.csp3 = 0
                }
              }
              this.csp = this.csp1 + this.csp2 + this.csp3
            }

            //// Compute Wh equivalent of values
            if (this.computeWH) {
              // Compute Wh equivalent
              const timeInterval = currentTime - this.mqttLastMessageDate

              //// Output power
              const outputWattHour = this.cow / (3600000 / timeInterval)
              this.tow += outputWattHour

              if (this.offGridHours()) {
                this.twg += outputWattHour
              }

              //// Solar power
              const solarWattHour = this.csp / (3600000 / timeInterval)
              this.tsp += (solarWattHour / 1000)
              this.spp += solarWattHour

              if (this.offGridHours() && this.cow > this.csp) {
                this.tbu += (this.cow - this.csp) / (3600000 / timeInterval)
              }
            }

            this.mqttLastMessageDate = currentTime
          }
        } catch (err) {
          if (err) this.log.warn(`MQTT Message Error - ${err.message || err}`)
        }
      }
    })
    this.mqtt.on('offline', () => {
      this.log.warn(`MQTT Offline`)
      this.mqttClientId = null
    })
    this.mqtt.on('error', (err) => {
      if (err) this.log.warn(`MQTT Error - ${err.message || err}`)
      this.mqttClientId = null
    })
  }

  this.notify()
  this.connectEcoFlowAPI()
}

EcoFlowLite.prototype.getServices = function () {
  return [this.informationService,
  this.batteryLevelService,
  this.solarPowerCurrentProduction,
  this.outputPowerConsumption,
  this.pv1CurrentProduction,
  this.pv2CurrentProduction,
  this.pv3CurrentProduction,
  this.batteryLevelAutomationService,
  this.cycleCountBattery0,
  this.cycleCountBattery1].concat(this.computeWH ? [
    this.totalSolarPowerProduction,
    this.solarPowerProduction,
    this.totalOutputPowerConsumption,
    this.totalBatteryUsage,
    this.totalWithoutGrid
  ] : [])
}

EcoFlowLite.prototype._getPower = function (callback) {
  callback(null, this.soc)
}

EcoFlowLite.prototype._getOutputPower = function (callback) {
  callback(null, this.cow || 0.0001)
}

EcoFlowLite.prototype._getTotalOutputPower = function (callback) {
  callback(null, this.tow || 0.0001)
}

EcoFlowLite.prototype._getTotalOutputPowerWithoutGrid = function (callback) {
  callback(null, this.twg || 0.0001)
}

EcoFlowLite.prototype._getTotalBatteryUsage = function (callback) {
  callback(null, this.tbu || 0.0001)
}

EcoFlowLite.prototype._getChargingState = function (callback) {
  callback(null, true)
}

EcoFlowLite.prototype._getSolarPowerProduction = function (callback) {
  callback(null, this.spp || 0.0001)
}

EcoFlowLite.prototype._getTotalSolarPowerProduction = function (callback) {
  callback(null, this.tsp || 0.0001)
}

EcoFlowLite.prototype._getSolarPowerCurrentProduction = function (callback) {
  callback(null, this.csp || 0.0001)
}

EcoFlowLite.prototype._getPV1CurrentProduction = function (callback) {
  callback(null, this.csp1 || 0.0001)
}

EcoFlowLite.prototype._getPV2CurrentProduction = function (callback) {
  callback(null, this.csp2 || 0.0001)
}

EcoFlowLite.prototype._getPV3CurrentProduction = function (callback) {
  callback(null, this.csp3 || 0.0001)
}

EcoFlowLite.prototype._getCycleCountBattery0 = function (callback) {
  callback(null, this.bcc0 || 0.0001)
}

EcoFlowLite.prototype._getCycleCountBattery1 = function (callback) {
  callback(null, this.bcc1 || 0.0001)
}

